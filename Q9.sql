SELECT
   ic.category_name,
   SUM(i.item_price) AS total_price
FROM
   item i
INNER JOIN
   item_category ic
ON
   ic.category_id = i.category_id
GROUP BY
   i.category_id
ORDER BY
   total_price DESC;
