SELECT
   i.item_id,
   i.item_name,
   i.item_price,
   ic.category_name
FROM
   item i
INNER JOIN
   item_category ic
ON
   ic.category_id = i.category_id;
